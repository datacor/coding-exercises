## Exercise 1
Datacor ERP is an Enterprise Resource Planning system that is designed for process manufacturers and chemical distributors.  An important function of this ERP is to provide dimensional anaylsis for various containers and raw materials.  This analysis is used to perform Unit of Measure (UOM) calculations that describe the relationships between different physical quantities by identifying their base quantities (such as gallons vs. liters, or pounds vs. kilograms) and tracking these dimensions as calculations or comparisons.  The conversion of units from one dimensional unit to another is performed frequently in the ERP, as batch formulas and supplier quantities are often in different units. 

For this exercise, you will need to write a program that can convert from one UOM to another (where possible).  Your program should accept:

### quantity:
a numeric value describing the quantity/amount of product

### source units type:
a desgnator indicating the unit of measure for the product

### desired units type:
a designator indicating the desired unit of measure for the conversion



##### Example Input: 3.8 L G
##### Output: 1.0 G
#
##### Example Input: 1.0 FT CM
##### Output: 30.48 CM
##

In the examples above, 3.8 Liters was converted to its equivalent in Gallons, which is 1.0; 1.0 feet was converted to its equivalent in centimeters, which is 30.48

Use the following UOM conversion factors for your program calculations

###### 1.0 Gallon (G) = 3.8 Liters (L)
###### 1.0 Inches (IN) = 2.54 Centimeters (CM)
###### 1.0 Gallon (G) = 8.0 Pounds (LB)
###### 1.0 Foot (FT) = 12.0 Inches (IN)
###### 1.0 Yard (YD) = 3.0 Feet (FT)
###### 1.0 Kilogram (KG) = 2.2 Pounds (LB)

### This program should be able to take quantity, source units, and destination units as input and display converted units as output. It's up to you how you implement that. Keep in mind that this conversion logic should be able to be reused by other applications in the future.
###